# devLib2 conda recipe

Home: "https://github.com/epics-modules/devlib2"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS devLib2 module
